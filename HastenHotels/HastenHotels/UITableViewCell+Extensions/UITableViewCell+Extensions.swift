//
//  UITableViewCell+Extensions.swift
//  HastenHotels
//
//  Created by manuel on 21/6/22.
//

import Foundation
import UIKit

extension UITableViewCell{
    class var className: String { "\(self)" }
}
