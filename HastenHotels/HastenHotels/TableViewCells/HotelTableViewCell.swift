//
//  HotelTableViewCell.swift
//  HastenHotels
//
//  Created by manuel on 21/6/22.
//

import UIKit

class HotelTableViewCell: UITableViewCell {

    @IBOutlet weak var imageHotelView: UIView!
    @IBOutlet weak var hotelNameLabel: UILabel!
    @IBOutlet weak var hotelRatingLabel: UILabel!
    
    var hotelNameString = ""{
        didSet{
            self.hotelNameLabel.text = "Nombre: \(hotelNameString)"
        }
    }
    
    var hotelRatingString = ""{
        didSet{
            self.hotelRatingLabel.text = "Rating: \(hotelRatingString)"
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
