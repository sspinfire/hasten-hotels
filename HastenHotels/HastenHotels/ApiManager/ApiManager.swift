//
//  ApiManager.swift
//  HastenHotels
//
//  Created by manuel on 20/6/22.
//

import Foundation

struct ApiManager{
    
    fileprivate static let multiRating = "\(demoUrl)/multi-ratings"
    fileprivate static let multiHotels = "\(demoUrl)/multi-hotels"

    static func getHotelsMultiRating(completionHandler: @escaping (Result<Hotels, Error>) -> Void){
        let url = "\(multiRating)"
        AFWrapper.sharedInstance.request(url: url, method: "GET") { (result) in
            completionHandler(result)
        }
    }
    
    static func getHotelsMultiHotels(completionHandler: @escaping (Result<Hotels, Error>) -> Void){
        let url = "\(multiHotels)"
        AFWrapper.sharedInstance.request(url: url, method: "GET") { (result) in
            completionHandler(result)
        }
    }
}
