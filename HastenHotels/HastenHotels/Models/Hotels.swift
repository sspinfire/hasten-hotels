//
//  Hotels.swift
//  HastenHotels
//
//  Created by manuel on 20/6/22.
//

import Foundation

struct Hotels: Codable {
    let hotels: [Hotel]
}

struct Hotel: Codable{
    let code: String
    let name: String?
    let address: String?
    let image: ImageHotel?
    var rating: Rating?
}

struct ImageHotel: Codable{
    let url: String
}

struct Rating: Codable{
    let bubbleRating: Double
    let reviewCount: Int
}
