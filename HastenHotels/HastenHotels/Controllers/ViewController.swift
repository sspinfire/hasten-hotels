//
//  ViewController.swift
//  HastenHotels
//
//  Created by manuel on 20/6/22.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var loandingIndicator: UIActivityIndicatorView!
    @IBOutlet weak var tableView: UITableView!    
    var hotelList: Hotels?

    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupView()
        self.getHotels()
    }

    private func setupView(){
        tableView.delegate = self
        tableView.dataSource = self
        loandingIndicator.startAnimating()
        loandingIndicator.isHidden = false
        loandingIndicator.hidesWhenStopped = true
        let hotelTableViewCell: UINib = UINib(nibName: HotelTableViewCell.className, bundle: nil)
        self.tableView.register(hotelTableViewCell, forCellReuseIdentifier: HotelTableViewCell.className)
    }
    
    private func getHotelsAndRatingSorted(hotelWithRating: Hotels, hotelWithoutRating: Hotels){
        DispatchQueue.main.async { [self] in
                let list = hotelWithoutRating.hotels.map ({ hotel -> Hotel in
                    let hotelsRating = hotelWithRating.hotels.filter({($0.code == hotel.code)})
                    var hot = hotel
                    hot.rating = hotelsRating[0].rating
                    return hot
                }).sorted {
                    $0.rating!.bubbleRating > $1.rating!.bubbleRating
                }
                self.hotelList = Hotels(hotels: list)
            loandingIndicator.stopAnimating()
            self.tableView.reloadData()
        }
    }
    
    private func getHotels(){
        ApiManager.getHotelsMultiHotels { result in
            switch result{
            case .success(let hotelWithoutRating):
                self.getHotelsRating(hotelWithoutRating: hotelWithoutRating)
            case .failure(let error):
                print("error hotel: \(error)")
            }
        }
    }
    
    private func getHotelsRating(hotelWithoutRating: Hotels){
        ApiManager.getHotelsMultiRating { [self] result in
            switch result{
            case .success(let hotelWithRating):
                self.getHotelsAndRatingSorted(hotelWithRating: hotelWithRating, hotelWithoutRating: hotelWithoutRating)
            case .failure(let error):
                print("error rating: \(error)")
            }
        }
    }
    
    
}

extension ViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
                let hotelsTableViewCell = self.tableView.dequeueReusableCell(withIdentifier: HotelTableViewCell.className) as! HotelTableViewCell
        if let hotelList = hotelList , let rating = hotelList.hotels[indexPath.row].rating, let name = hotelList.hotels[indexPath.row].name{
            hotelsTableViewCell.hotelRatingString = "\(rating.bubbleRating)"
                    hotelsTableViewCell.hotelNameString = name
        }
        return hotelsTableViewCell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let count = hotelList?.hotels.count else {
            return 0
        }
        return count
    }
}
